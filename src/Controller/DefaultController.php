<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController extends AbstractController
{
    /**
     * @Route("/",
     *      name="default",
     *      methods={"GET"}
     * )
     */
    public function default() : Response
    {
        $response = [
            'message' => 'OK',
        ];
        return $this->json($response, 200);
    }
}